#include <stdio.h>

struct student{
	char fname[50];
	char subj[50];
	int marks;
};

int main()
{
	int i=0,n;
	
	printf("Enter number of students : \n");
	scanf("%d", &n); 
	
	struct student s[n];
	
	printf("Enter the student information ");
	printf("\n");
	
	for(i ; i<n ; i++)
	{
		printf("Enter student name : ");
		scanf("%s", s[i].fname);
		printf("Enter the subject: ");
		scanf("%s", s[i].subj);
		printf("Enter the marks : ");
		scanf("%d", &s[i].marks);	
		printf("\n");
	}
	
	printf("Student information: \n");
	printf("\n");
	
	i=0;	
	for(i ; i<n ; i++)
	{
		printf("Student name: %s \n", s[i].fname);
		printf("Subject: %s \n", s[i].subj);
		printf("Marks %d \n", s[i].marks);
		printf("\n");
		}	
		
	return 0;
}
